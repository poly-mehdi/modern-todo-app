import Header from './components/Header'
import Form from './components/Form'
import { useEffect, useState } from 'react'
import { nanoid } from 'nanoid'
import Tasks from './components/Tasks'
import SelectionTask from './components/SelectionTask'
import Footer from './components/Footer'
import { templateTasks } from './utils'
import lightBackground from './images/bg-desktop-light.jpg'
import darkBackground from './images/bg-desktop-dark.jpg'

const getLocalStorage = () => {
  let list = localStorage.getItem('list')
  return list ? JSON.parse(list) : []
}

const setLocalStorage = (tasks) => {
  localStorage.setItem('list', JSON.stringify(tasks))
}
const defaultList = getLocalStorage()

function App() {
  // useState
  const [theme, setTheme] = useState('dark-theme')
  const [tasks, setTasks] = useState(defaultList)
  const [hasCompletedTasks, setHasCompletedTasks] = useState(false)
  const [totalItems, setTotalItems] = useState(defaultList.length)
  // functions

  const addTask = (taskName) => {
    const tasksStored = getLocalStorage()
    const newTask = {
      name: taskName,
      completed: false,
      id: nanoid(),
    }
    const newTasks = [...tasksStored, newTask]
    setTasks(newTasks)
    setLocalStorage(newTasks)
  }

  const editTask = (taskId) => {
    const tasksStored = getLocalStorage()
    const newTasks = tasksStored.map((task) => {
      if (task.id === taskId) {
        const newTask = { ...task, completed: !task.completed }
        return newTask
      }
      return task
    })
    setTasks(newTasks)
    setLocalStorage(newTasks)
  }

  const removeTask = (taskId) => {
    const tasksStored = getLocalStorage()
    const newTasks = tasksStored.filter((task) => task.id !== taskId)
    setTasks(newTasks)
    setLocalStorage(newTasks)
  }

  const displayTasks = (select) => {
    if (select === 'all') {
      const tasksStored = getLocalStorage()
      setTasks(tasksStored)
    } else if (select === 'active') {
      const tasksStored = getLocalStorage()
      const activeTasks = tasksStored.filter((task) => task.completed === false)
      setTasks(activeTasks)
    } else {
      const tasksStored = getLocalStorage()
      const completedTasks = tasksStored.filter(
        (task) => task.completed === true
      )
      setTasks(completedTasks)
    }
  }

  const findCompletedTasks = () => {
    const tasksStored = getLocalStorage()
    const completedTasks = tasksStored.find((task) => task.completed === true)
    if (completedTasks) {
      setHasCompletedTasks(true)
    } else {
      setHasCompletedTasks(false)
    }
  }

  const clearCompletedTasks = (select) => {
    const tasksStored = getLocalStorage()

    const newTasks = tasksStored.filter((task) => task.completed === false)
    setLocalStorage(newTasks)

    if (select === 'completed') {
      setTasks([])
    } else {
      setTasks(newTasks)
    }
  }

  const toggleTheme = () => {
    theme === 'light-theme' ? setTheme('dark-theme') : setTheme('light-theme')
  }
  // useEffect
  useEffect(() => {
    findCompletedTasks()
    setTotalItems(getLocalStorage().length)
  }, [tasks])

  useEffect(() => {
    document.documentElement.className = theme
    document.body.style.backgroundImage = `url(${
      theme === 'dark-theme' ? darkBackground : lightBackground
    })`
  }, [theme])

  return (
    <main>
      <section className="todo-container">
        {/* <ToastContainer position="top-center" /> */}
        <Header toggleTheme={toggleTheme} theme={theme} />
        <Form addTask={addTask} />
        <Tasks
          tasks={tasks}
          editTask={editTask}
          removeTask={removeTask}
          theme={theme}
          setLocalStorage={setLocalStorage}
        />
        <SelectionTask
          hasCompletedTasks={hasCompletedTasks}
          totalItems={totalItems}
          clearCompletedTasks={clearCompletedTasks}
          displayTasks={displayTasks}
        />
        <Footer />
      </section>
    </main>
  )
}

export default App
