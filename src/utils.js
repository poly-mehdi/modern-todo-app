export const templateTasks = [
  {
    name: 'Complete online JavaScript course',
    completed: true,
    id: 1,
  },
  {
    name: 'Jog around the park 3x',
    completed: false,
    id: 2,
  },
  {
    name: '10 minutes meditation',
    completed: false,
    id: 3,
  },
  {
    name: 'Read for 1 hour',
    completed: false,
    id: 4,
  },
  {
    name: 'Pick up groceries',
    completed: false,
    id: 5,
  },
  {
    name: 'Complete Todo App on Frontend Mentor',
    completed: false,
    id: 6,
  },
]
