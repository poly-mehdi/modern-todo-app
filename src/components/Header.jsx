import iconMoon from '../images/icon-moon.svg'
import iconSun from '../images/icon-sun.svg'

const Header = ({ toggleTheme, theme }) => {
  return (
    <div className="header">
      <h1>TODO</h1>
      <img
        src={theme === 'dark-theme' ? iconSun : iconMoon}
        alt="icon-moon"
        className="light-mode"
        onClick={toggleTheme}
      />
    </div>
  )
}
export default Header
