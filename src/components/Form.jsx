import { useState } from 'react'

const Form = ({ addTask }) => {
  const [newTaskName, setNewTaskName] = useState('')

  const handleSubmit = (e) => {
    e.preventDefault()
    if (!newTaskName) {
      return
    }
    addTask(newTaskName)
    setNewTaskName('')
  }
  return (
    <form onSubmit={handleSubmit}>
      <div className="form-control">
        <input
          type="text "
          className="form-input"
          autoCapitalize="off"
          placeholder="Create a new todo..."
          value={newTaskName}
          onChange={(event) => setNewTaskName(event.target.value)}
        />
        <button type="submit" className="form-btn"></button>
      </div>
    </form>
  )
}
export default Form
