import iconCross from '../images/icon-cross.svg'
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd'

const SingleTask = ({ task, editTask, removeTask, provided, theme }) => {
  return (
    <li
      className="single-task"
      {...provided.draggableProps}
      {...provided.dragHandleProps}
      ref={provided.innerRef}
    >
      <div className="single-task-content">
        <div className="checkbox-container">
          <input
            type="checkbox"
            name={`check${task.id}`}
            checked={task.completed}
            onChange={() => editTask(task.id)}
          />
          <label
            htmlFor={`check${task.id}`}
            onClick={() => editTask(task.id)}
            style={{
              textDecoration: task.completed && 'line-through',
              color:
                task.completed && theme === 'light-theme'
                  ? task.completed && 'hsl(233, 11%, 84%)'
                  : task.completed && 'hsl(233, 14%, 35%)',
            }}
          >
            {task.name}
          </label>
        </div>
      </div>
      <button className="task-cross-btn">
        <img
          src={iconCross}
          alt="iconCross"
          className="task-cross"
          onClick={() => {
            removeTask(task.id)
          }}
        />
      </button>
    </li>
  )
}
export default SingleTask
