import React, { useEffect, useState } from 'react'
import SingleTask from './SingleTask'
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd'

const Tasks = ({ tasks, editTask, removeTask, theme, setLocalStorage }) => {
  const [currentTasks, setCurrentTasks] = useState(tasks)

  const handleOnDragEnd = (result) => {
    const items = Array.from(currentTasks)
    const [reorderedItem] = items.splice(result.source.index, 1)
    items.splice(result.destination.index, 0, reorderedItem)
    setCurrentTasks(items)
    setLocalStorage(items)
  }

  useEffect(() => {
    setCurrentTasks(tasks)
  }, [tasks])

  return (
    <DragDropContext onDragEnd={handleOnDragEnd}>
      <Droppable droppableId="tasks">
        {(provided) => {
          return (
            <ul
              className="tasks"
              {...provided.droppableProps}
              ref={provided.innerRef}
            >
              {currentTasks.length !== 0 ? (
                currentTasks.map((task, index) => {
                  return (
                    <Draggable
                      key={task.id}
                      draggableId={task.id}
                      index={index}
                    >
                      {(provided) => {
                        return (
                          <SingleTask
                            key={task.id}
                            task={task}
                            theme={theme}
                            editTask={editTask}
                            removeTask={removeTask}
                            provided={provided}
                          />
                        )
                      }}
                    </Draggable>
                  )
                })
              ) : (
                <div className="no-tasks">
                  <p>No Tasks</p>
                </div>
              )}
              {provided.placeholder}
            </ul>
          )
        }}
      </Droppable>
    </DragDropContext>
  )
}

export default Tasks
