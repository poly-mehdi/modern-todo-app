import { useState, useEffect } from 'react'

const SelectionTask = ({
  displayTasks,
  hasCompletedTasks,
  clearCompletedTasks,
  totalItems,
}) => {
  const [selectedTask, setSelectedTask] = useState('all')
  const [windowWidth, setWindowWidth] = useState(window.innerWidth)

  const handleClick = (task) => {
    setSelectedTask(task)
  }

  const handleResize = () => {
    setWindowWidth(window.innerWidth)
  }

  useEffect(() => {
    window.addEventListener('resize', handleResize)

    return () => {
      window.removeEventListener('resize', handleResize)
    }
  }, [])

  if (windowWidth < 1024) {
    return (
      <>
        <div className="tasks-info">
          <p>
            {totalItems} item{totalItems > 1 ? 's' : ''} left
          </p>
          {hasCompletedTasks && (
            <p
              className="tasks-info-completed"
              onClick={() => clearCompletedTasks(selectedTask)}
            >
              Clear Completed
            </p>
          )}
        </div>

        <section className="selection-task">
          <div className="selection-task-center">
            <p
              onClick={() => {
                displayTasks('all')
                handleClick('all')
                setSelectedTask('all')
              }}
              className={
                selectedTask === 'all' ? 'selection-task-selected' : ''
              }
            >
              All
            </p>
            <p
              onClick={() => {
                displayTasks('active')
                handleClick('active')
                setSelectedTask('active')
              }}
              className={
                selectedTask === 'active' ? 'selection-task-selected' : ''
              }
            >
              Active
            </p>
            <p
              onClick={() => {
                displayTasks('completed')
                handleClick('completed')
                setSelectedTask('completed')
              }}
              className={
                selectedTask === 'completed' ? 'selection-task-selected' : ''
              }
            >
              Completed
            </p>
          </div>
        </section>
      </>
    )
  } else {
    return (
      <div className="tasks-info-wide">
        <p>
          {totalItems} item{totalItems > 1 ? 's' : ''} left
        </p>
        <section className="selection-task-wide">
          <div className="selection-task-center-wide">
            <p
              onClick={() => {
                displayTasks('all')
                handleClick('all')
                setSelectedTask('all')
              }}
              className={
                selectedTask === 'all' ? 'selection-task-selected' : ''
              }
            >
              All
            </p>
            <p
              onClick={() => {
                displayTasks('active')
                handleClick('active')
                setSelectedTask('active')
              }}
              className={
                selectedTask === 'active' ? 'selection-task-selected' : ''
              }
            >
              Active
            </p>
            <p
              onClick={() => {
                displayTasks('completed')
                handleClick('completed')
                setSelectedTask('completed')
              }}
              className={
                selectedTask === 'completed' ? 'selection-task-selected' : ''
              }
            >
              Completed
            </p>
          </div>
        </section>
        {hasCompletedTasks ? (
          <p
            className="tasks-info-completed"
            onClick={() => clearCompletedTasks(selectedTask)}
          >
            Clear Completed
          </p>
        ) : (
          <p className="completed-empty"></p>
        )}
      </div>
    )
  }
}
export default SelectionTask
