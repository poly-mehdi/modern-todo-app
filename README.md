# Frontend Mentor - Todo app solution

This is a solution to the [Todo app challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/todo-app-Su1_KokOW). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Useful resources](#useful-resources)
- [Author](#author)

## Overview

### The challenge

Users should be able to:

- View the optimal layout for the app depending on their device's screen size
- See hover states for all interactive elements on the page
- Add new todos to the list
- Mark todos as complete
- Delete todos from the list
- Filter by all/active/complete todos
- Clear all completed todos
- Toggle light and dark mode
- Drag and drop to reorder items on the list

### Screenshot

![Mobile Design Dark Mode](./design/mobile-design-dark.png)
![Mobile Design Light Mode](./design/mobile-design-light.png)
![Desktop Design Dark Mode](./design/desktop-design-dark.png)
![Desktop Design Light Mode](./design/desktop-design-light.png)

### Links

- Solution URL: [Modern TODO App](https://modern-todo-app-poly-mehdi.vercel.app/)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- CSS Grid
- Mobile-first workflow
- [React](https://reactjs.org/) - JS library
- [Vite.js](https://vitejs.dev/) - React framework

### What I learned

During the development of this project, I gained valuable experience in implementing drag and drop functionality using the React Beautiful Drag and Drop library. This allowed me to enhance the user experience by enabling intuitive task rearrangement. Additionally, I had the opportunity to deepen my knowledge of various aspects of React, such as utilizing hooks like useEffect and useState for managing component state, handling prop drilling to efficiently pass data between components, dynamically rendering components based on screen size, and applying CSS styles to customize checkbox elements and scrollbars. This project has been a great learning experience, enabling me to expand my skills in React and frontend development.

### Useful resources

- [DND Tutorial](https://www.youtube.com/watch?v=aYZRRyukuIw&ab_channel=ColbyFayock) - This is an amazing video which helped me to implement drag and drop.

## Author

- Website - [Mon Portofolio](https://portofolio-poly-mehdi.vercel.app/)
- Frontend Mentor - [@poly-mehdi](https://www.frontendmentor.io/profile/poly-mehdi)
